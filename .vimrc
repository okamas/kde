"""""""""
"Plugins"
"""""""""
" NERDTree Plugin
" open automatically when vim starts up
"autocmd vimenter * NERDTree

"""""""""""""""""
"Plugin Features"
"""""""""""""""""
map <C-n> :NERDTreeToggle<CR>

"""""""""""""""""
"Vim-Plug""""""""
"""""""""""""""""
call plug#begin()
Plug 'preservim/nerdtree'
call plug#end()
Plug 'ctrlpvim/ctrlp.vim'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'vim-airline/vim-airline'

let g:airline_theme='simple'

set runtimepath^=~/.vim/plugged/ctrlp.vim
set guifont=Iosevka\ Term\ 11

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


"colorscheme gruvbox 

"packadd! dracula

"autocmd vimenter * ++nested colorscheme gruvbox

"set background=dark " Setting light mode
""""""""
"THEMES"
""""""""

""""""""
"Others"
""""""""
filetype plugin on
" Set visualbell
set mouse=n
" Set compatibility to Vim only.
"set nocompatible

" Visual options
set cursorline " highlight current line
"set cursorcolumn " highlight current cloumn
set cursorline
"set cursorcolumn

fu! ToggleCurline ()
    if &cursorlin && &cursorcolumn
        set nocursorline
        set nocursorcolumn
    else
        set cursorline
        set cursorcolumn
    endif
endfunction

"Always show current position
set ruler

" Turn on syntax highlighting.
syntax on
syntax enable

" Turn off modelines
set modelines=0

" Uncomment below to set the max textwidth. Use a value corresponding to the width of your screen.
" set textwidth=80
set formatoptions=tcqrn1
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set noshiftround

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Display 5 lines above/below the cursor when scrolling with a mouse.
set scrolloff=5
" Fixes common backspace problems
set backspace=indent,eol,start

" Display options
set showmode
set showcmd
set cmdheight=1

" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>

" Display different types of white spaces.
"set list
"set listchars=tab:›\ ,trail:•,extends:#,nbsp:.

" Show line numbers
set number
highlight LineNr ctermfg=white

" Set status line display
"set laststatus=2
"hi StatusLine ctermfg=NONE ctermbg=red cterm=NONE
"hi StatusLineNC ctermfg=black ctermbg=red cterm=NONE
"hi User1 ctermfg=black ctermbg=magenta
"hi User2 ctermfg=NONE ctermbg=NONE
"hi User3 ctermfg=black ctermbg=blue
"hi User4 ctermfg=black ctermbg=cyan
"set statusline=\                    " Padding
"set statusline+=%f                  " Path to the file
"set statusline+=\ %1*\              " Padding & switch colour
"set statusline+=%y                  " File type
"set statusline+=\ %2*\              " Padding & switch colour
"set statusline+=%=                  " Switch to right-side
"set statusline+=\ %3*\              " Padding & switch colour
"set statusline+=line                " of Text
"set statusline+=\                   " Padding
"set statusline+=%l                  " Current line
"set statusline+=\ %4*\              " Padding & switch colour
"set statusline+=of                  " of Text
"set statusline+=\                   " Padding
"set statusline+=%L                  " Total line
"set statusline+=\                   " Padding

" Encoding
set encoding=utf-8

" Highlight matching search patterns
"set hlsearch

" Enable incremental search
"set incsearch

" Store info from no more than 100 files at a time, 9999 lines of text
" 100kb of data. Useful for copying large amounts of data between files.
set viminfo='100,<9999,s100
