#
#GG ~/.bashrc
#
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export VISUAL=nvim
export EDITOR="$VISUAL"

PATH="/home/rich/.local/share/gem/ruby/3.0.0/bin:$PATH"
export PATH

if [ -n "$RANGER_LEVEL" ]; then export PS1="[ranger]$PS1"; fi

alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '

# prompt from siduck76
PS1="\[\033[32m\]  \[\033[37m\]\[\033[34m\]\w \[\033[0m\]"
PS2="\[\033[32m\]  > \[\033[0m\]"

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing

# Adding flags
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB

# Pacman & Yay
alias pacl="pacman -Qq | fzf --preview 'pacman -Qil {}' --layout=reverse --bind 'enter:execute(pacman -Qil {} | less)'"
alias p="sudo pacman -S"
alias prs="sudo pacman -Rs"
alias prns="sudo pacman -Rns"
alias pss="pacman -Ss"
alias sp="sudo pacman"
alias pp="pacman"
alias sv="sudo nvim"
alias v="nvim"
alias pcup="sudo pacman -Syu"
alias m="cmus"
alias r="ranger"
alias k="killall"
alias neo="neofetch"

# youtube-dl

alias yt='youtube-dl --extract-audio --add-metadata --xattrs --embed-thumbnail --audio-quality 0 --audio-format mp3'

alias ytv='youtube-dl --merge-output-format mp4 -f "bestvideo+bestaudio[ext=m4a]/best" --embed-thumbnail --add-metadata'

# shutdown or reboot
alias ssn="sudo shutdown now"
alias srt="sudo reboot"

# Merge Xresources
alias merge='xrdb -merge ~/.Xresources'

# Flatpak Apps
alias dis='flatpak run com.discordapp.Discord'
alias ko=

# PixivUtil2
alias ppu='cd ~/Projects/PixivUtil2/ && python PixivUtil2.py'

# Mat2 Metadata Clener
alias cms='mat2 -s'
alias cm='mat2'
alias cmi='mat2 --inplace --verbose'
# Git
alias gc='git clone'
# youtube-dl
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias ytv-best="youtube-dl -f bestvideo+bestaudio "
# ytdl
#alias ytmdl

alias gpipe="gtk-pipe-viewer"
alias pipev="pipe-viewer"

# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

#lightnovel-crawler
# Multi (build seperate ebooks by volumes)
alias lncm="lncrawl --multi --format epub -s"
# Volumes (download specific volumes)
alias lncv="lncrawl --volumes -format epub -s"
# for novels that doesn't come segmented in a volume list but rather
# in  larger only chapter list which means that the .epub file will
# be a larger one resulting in a decrease speed of Okular.
alias lncs="lncrawl --single -i --format epub --suppress -s"

# nvim Shortcuts
alias vd1="nvim ~/Documents/doujinshi1.txt"
alias vd="nvim ~/Documents/doujinshi.txt"

# Redshift
alias rs="redshift -O 2500K"
alias rx="redshift -x"

# nhentai
alias nhen="nhentai --format '%t' -C --rm-origin-dir -o ~/Pictures/nhcbzTmp/ -f ~/Documents/doujinshi.txt"
alias nheni="nhentai --format '%t' -C --rm-origin-dir -o ~/Pictures/nhcbzTmp/"

alias xmd="xed /home/rich/.var/app/io.github.hakuneko.HakuNeko/cache/hakuneko-desktop/mjs/connectors"
alias kmd="kate /home/rich/.var/app/io.github.hakuneko.HakuNeko/cache/hakuneo-desktop/mjs/connectors/MangaDex.mjs"
alias pinga="ping -c 5 archlinux.org"
alias fd="find . -type d -name"
alias ff="find . -type f -name"
alias zrc="nvim ~/.zshrc"

### ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}
