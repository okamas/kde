[Appearance]
BoldIntense=true
ColorScheme=Gruvbox Dark
Font=Fira Code Retina,9,-1,5,53,0,0,0,0,0,Regular
UseFontLineChararacters=true

[Encoding Options]
DefaultEncoding=UTF-8

[General]
Command=/bin/zsh
CursorShape=50
Environment=TERM=xterm-256color,COLORTERM=truecolor
Name=Shell
Parent=FALLBACK/
TerminalMargin=4
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true

[Interaction Options]
UnderlineFilesEnabled=true

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=2
