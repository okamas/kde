[Appearance]
ColorScheme=Gruvbox Dark
Font=Fira Code,9,-1,5,50,0,0,0,0,0

[General]
Command=/usr/bin/zsh
Environment=TERM=xterm-256color,COLORTERM=truecolor
Name=Profile 1
Parent=FALLBACK/

[Scrolling]
HistoryMode=2
